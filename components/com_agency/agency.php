<?php
/**
 * @version     1.0.0
 * @package     com_agency
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Dfunl <info@dfunl.com> - http://www.dfunl.com
 */

defined('_JEXEC') or die;

// Include dependancies
jimport('joomla.application.component.controller');

// Execute the task.
$controller	= JControllerLegacy::getInstance('Agency');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
