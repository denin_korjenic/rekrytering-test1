<?php

/**
 * @version     1.0.0
 * @package     com_agency
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Dfunl <info@dfunl.com> - http://www.dfunl.com
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');
jimport('joomla.filesystem.file');

class AgencyController extends JControllerLegacy {

    /**
     * Method to display a view.
     *
     * @param	boolean			$cachable	If true, the view output will be cached
     * @param	array			$urlparams	An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
     *
     * @return	JController		This object to support chaining.
     * @since	1.5
     */
    public function display($cachable = false, $urlparams = false) {
        require_once JPATH_COMPONENT . '/helpers/agency.php';

        $view = JFactory::getApplication()->input->getCmd('view', 'lists');
        JFactory::getApplication()->input->set('view', $view);

        parent::display($cachable, $urlparams);

        return $this;
    }

    public function AddNewProfile() {
      $app = JFactory::getApplication();
      $input = $app->input;

      $model = $this->getModel('editprofile');

      $name = $input->getVar("firstname");
      $email = $input->getVar("email");


      $userid = $model->addNewUser($name, $email);
      if ($userid) {




      $profile = new stdClass();

      $profile->userid = $userid;

      $profile->lastname = $input->getVar("lastname");

      $profile->address = $input->getVar("address");

      $profile->zip = $input->getVar("zip");

      $profile->country = $input->getVar("country");

      $profile->phone = $input->getVar("phone");

      $profile->city = $input->getVar("city");

      $profile->aditional = $input->getVar("aditional");
      $profile->terms = $input->getVar("terms");
      $profile->gender = $input->getVar("gender");
      $profile->eduone = $input->getVar("eduone");
      $profile->edutwo = $input->getVar("edutwo");
      $profile->eduthree = $input->getVar("eduthree");
      $profile->keywords = $input->getVar("keywords");


      $destination = "";

  	//	foreach ($input->files->get("cv") as $key => $file) {
      $file = $input->files->get("cv");
  			if (strlen($file["name"])) {
  				$destination = "images/profile_" . $userid . "_" . $file["name"];

  				$i = 0;
  				while(file_exists($destination)) {
  					$destination = "images/profile_" . $userid ."_".$i. "_" . $file["name"];
  					$i++;
  				}


  				JFile::upload($file['tmp_name'], $destination);
  			}

  //		}

      $profile->cv = $destination;


      $destination = "";

  	//	foreach ($input->files->get("letter") as $key => $file) {
        $file = $input->files->get("letter");

  			if (strlen($file["name"])) {
  				$destination = "images/profile_" . $userid . "_" . $file["name"];

  				$i = 0;
  				while(file_exists($destination)) {
  					$destination = "images/profile_" . $userid . "_" . $i . "_" . $file["name"];
  					$i++;
  				}


  				JFile::upload($file['tmp_name'], $destination);
  			}

      $profile->letter = $destination;

  	//	}

        $file = $input->files->get("image");

        $destination = "";
        if (strlen($file["name"])) {
          $destination = "images/profile_" . $userid . "_" . $file["name"];

          $i = 0;
          while (file_exists($destination)) {
            $destination = "images/profile_" . $userid . "_" . $i . "_" . $file["name"];
            $i++;
          }

          JFile::upload($file['tmp_name'], $destination);
          $profile->image = $destination;
        }

      
      

      $profile->presentation = $input->getVar("presentation");

      $profile->comments = "";
      
      $profile->comments_counter = $comments_counter;

      $model->addNewProfile($profile);



      $experience = new stdClass();
      $experience->employer = $input->getVar("employer");
      $experience->position_title = $input->getVar("positiontitle");
      $experience->start_datemonth = $input->getVar("startdatemonth");
      $experience->start_dateyear = $input->getVar("startdateyear");
      $experience->end_datemonth = $input->getVar("enddatemonth");
      $experience->end_dateyear = $input->getVar("enddateyear");

      $model->addNewExperience($userid, $experience);

      $app->enqueueMessage('Successfully created');
    }
    else {
      $app = JFactory::getApplication();

      $app->enqueueMessage('User exists');

    }

    }

    public function updateProfile() {
      $app = JFactory::getApplication();
      $input = $app->input;

      $model = $this->getModel('editprofile');

      $name = $input->getVar("firstname");
      $email = $input->getVar("email");
      $uid = $input->getVar("profid");


      $userid = $model->updateUser($name, $email, $uid);
      if ($userid) {


  

        $profile = new stdClass();

        $profile->userid = $userid;

        $profile->lastname = $input->getVar("lastname");

        $profile->address = $input->getVar("address");

        $profile->zip = $input->getVar("zip");

        $profile->country = $input->getVar("country");

        $profile->phone = $input->getVar("phone");

        $profile->city = $input->getVar("city");

        $profile->aditional = $input->getVar("aditional");
        $profile->terms = $input->getVar("terms");
        $profile->gender = $input->getVar("gender");
        $profile->eduone = $input->getVar("eduone");
        $profile->edutwo = $input->getVar("edutwo");
        $profile->eduthree = $input->getVar("eduthree");
        $profile->keywords = $input->getVar("keywords");


        $destination = "";

      //  foreach ($input->files->get("cv") as $key => $file) {
        $file = $input->files->get("cv");
          if (strlen($file["name"])) {
            $destination = "images/profile_" . $userid . "_" . $file["name"];

            $i = 0;
            while(file_exists($destination)) {
              $destination = "images/profile_" . $userid ."_".$i. "_" . $file["name"];
              $i++;
            }


            JFile::upload($file['tmp_name'], $destination);
            $profile->cv = $destination;
          }

    //    }

        $destination = "";

      //  foreach ($input->files->get("letter") as $key => $file) {
          $file = $input->files->get("letter");

          if (strlen($file["name"])) {
            $destination = "images/profile_" . $userid . "_" . $file["name"];

            $i = 0;
            while(file_exists($destionation)) {
              $destination = "images/profile_" . $userid . "_" . $i . "_" . $file["name"];
              $i++;
            }


            JFile::upload($file['tmp_name'], $destination);
            $profile->letter = $destination;
          }

      //  }

          $file = $input->files->get("image");
          
          $destination = "";

          if (strlen($file["name"])) {
            $destination = "images/profile_" . $userid . "_" . $file["name"];

            $i = 0;
            while (file_exists($destination)) {
              $destination = "images/profile_" . $userid . "_" . $i . "_" . $file["name"];
              $i++;
            }

            JFile::upload($file['tmp_name'], $destination);

            $profile->image = $destination;
          }

        $profile->presentation = $input->getVar("presentation");

        $profile->comments = "";

        $model->updateProfile($profile);

        $experience = new stdClass();
        $experience->employer = $input->getVar("employer", array(), 'ARRAY');
        $experience->position_title = $input->getVar("position_title", array(), 'ARRAY');
        $experience->start_datemonth = $input->getVar("start_datemonth", array(), 'ARRAY');
        $experience->start_dateyear = $input->getVar("start_dateyear", array(), 'ARRAY');
        $experience->end_datemonth = $input->getVar("end_datemonth", array(), 'ARRAY');
        $experience->end_dateyear = $input->getVar("end_dateyear", array(), 'ARRAY');
        //print_r($experience);

        $model->updateExperience($userid, $experience);

        $app->enqueueMessage('Successfully updated');
      }
      else {
        $app = JFactory::getApplication();

        $app->enqueueMessage('User exists');

      }

    }

    public function AddNewComment() {

      $app = JFactory::getApplication();
      $input = $app->input;

      $comment = $input->getVar("comment");

      $pid = $input->getVar("profid");

      $model = $this->getModel('list');

      $model->insertComment($comment, $pid);

      $app->redirect(JURI::root().'client-list#comments'.$pid);
    }


    public function AddNewExternalProfile() {
      $app = JFactory::getApplication();
      $input = $app->input;

      $model = $this->getModel('home');

      $name = $input->getVar("firstname");
      $email = $input->getVar("email");


      $userid = $model->addNewUser($name, $email);
      if ($userid) {




      $profile = new stdClass();

      $profile->userid = $userid;

      $profile->lastname = $input->getVar("lastname");

      $profile->address = $input->getVar("address");

      $profile->zip = $input->getVar("zip");

      $profile->country = $input->getVar("country");

      $profile->phone = $input->getVar("phone");

      $profile->city = $input->getVar("city");

      $profile->aditional = $input->getVar("aditional");
      $profile->terms = $input->getVar("terms");
      $profile->gender = $input->getVar("gender");
      $profile->eduone = $input->getVar("eduone");
      $profile->edutwo = $input->getVar("edutwo");
      $profile->eduthree = $input->getVar("eduthree");

      $destination = "";

    //  foreach ($input->files->get("cv") as $key => $file) {
      $file = $input->files->get("cv");
        if (strlen($file["name"])) {
          $destination = "images/profile_" . $userid . "_" . $file["name"];

          $i = 0;
          while(file_exists($destination)) {
            $destination = "images/profile_" . $userid ."_".$i. "_" . $file["name"];
            $i++;
          }


          JFile::upload($file['tmp_name'], $destination);
        }

  //    }

      $profile->cv = $destination;


      $destination = "";

    //  foreach ($input->files->get("letter") as $key => $file) {
        $file = $input->files->get("letter");

        if (strlen($file["name"])) {
          $destination = "images/profile_" . $userid . "_" . $file["name"];

          $i = 0;
          while(file_exists($destination)) {
            $destination = "images/profile_" . $userid . "_" . $i . "_" . $file["name"];
            $i++;
          }


          JFile::upload($file['tmp_name'], $destination);
        }

      $profile->letter = $destination;

    //  }

        $file = $input->files->get("image");
        
        $destination = "";
        
        if (strlen($file["name"])) {
          $destination = "images/profile_" . $userid . "_" . $file["name"];

          $i = 0;
          while (file_exists($destination)) {
            $destination = "images/profile_" . $userid . "_" . $i . "_" . $file["name"];
            $i++;
          }

          JFile::upload($file['tmp_name'], $destination);
          $profile->image = $destination;
        }

      
      

      $profile->presentation = $input->getVar("presentation");

      $profile->comments = "";


      $model->addNewProfile($profile);



      $experience = new stdClass();
      $experience->employer = $input->getVar("employer");
      $experience->position_title = $input->getVar("positiontitle");
      $experience->start_datemonth = $input->getVar("startdatemonth");
      $experience->start_dateyear = $input->getVar("startdateyear");
      $experience->end_datemonth = $input->getVar("enddatemonth");
      $experience->end_dateyear = $input->getVar("enddateyear");

      $model->addNewExperience($userid, $experience);

      $app->enqueueMessage('Successfully created');
    }
    else {
      $app = JFactory::getApplication();

      $app->enqueueMessage('User exists');

    }

    }
    public function proveUserExists() {
      $app = JFactory::getApplication();
      $input = $app->input;

      $model = $this->getModel('home');

      $name = $input->getVar("firstname");
      $email = $input->getVar("email");

      $r = $model->proveUser($name, $email);

      echo json_encode($r);
      exit;
    }

    public function removeProfileImage() {

      $app = JFactory::getApplication();
      $input = $app->input;

      $pid = $input->getVar("profid");

      $model = $this->getModel('profile');

      $model->removeProfileImage($pid);

      $app->redirect(JURI::root().'client-list/edit?profile='.$pid);
    }
    public function removeProfileLetter() {

      $app = JFactory::getApplication();
      $input = $app->input;

      $pid = $input->getVar("profid");

      $model = $this->getModel('profile');

      $model->removeProfileLetter($pid);

      $app->redirect(JURI::root().'client-list/edit?profile='.$pid);
    }
    public function removeProfileCV() {

      $app = JFactory::getApplication();
      $input = $app->input;

      $pid = $input->getVar("profid");

      $model = $this->getModel('profile');

      $model->removeProfileCV($pid);

      $app->redirect(JURI::root().'client-list/edit?profile='.$pid);
    }

}
