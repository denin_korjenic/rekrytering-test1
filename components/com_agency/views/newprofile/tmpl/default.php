
<script type="text/javascript">
function auto_grow(element) {
					element.style.height = "5px";
					element.style.height = (element.scrollHeight)+"px";
			}
</script>


<div class="edit-profile">
	<div class="edit-profile-header"><h1>Create your profile</h1></div>
	<div class="profile-info-container">

		<form id="edit-profile" method="post" enctype="multipart/form-data">

			<div class="part-form">
				<h3> 1. Personal information</h3>

				<div class="form-container">

						<span>Gender</span>
						<input type="radio" name="gender" value="male">Male
						<input type="radio" name="gender" value="female">Female

						<span>First Name</span>
						<input type="text" name="firstname">

						<span>Last Name</span>
						<input type="text" name="lastname">

						<span>Address</span>
						<textarea class="longInput" name="address"></textarea>

						<span>Zip code</span>
						<input type="text" name="zip">

						<span>Place of residence</span>
						<select name="country">
							<option>
								-- Country --
							</option>
							<?php
								foreach ($this->countrylist as $country) {
									echo "<option value=" . $country->country_code . ">" . $country->country_name . "</option>";
								}
							 ?>

						</select>

						<span>City</span>
						<input class="float-right" type="text" name="city">



						<span>Email</span>
						<input class="float-right" type="text" name="email">
						
						<span>Phone</span>
						<input class="float-right" type="text" name="phone">

						<span>Additional info</span>
						<textarea class="float-right" name="aditional"></textarea>

				</div>

				<div class="info-container">
<b>Hi and welcome to SCS Search - Management Recruiting in Life Science.</b> <br />If you are new to this site please start with creating an account and submitting your personal information. Your login information will be sent to your e-mail.<br /><br /> Please note that you can not use an e-mail that is already registered with us. <br /><br />If you have any questions please contact us by sending an e-mail to <a href="mailto:info@scssearch.com">info@scssearch.com</a>


				</div>

				<div class="form-clr"></div>

			</div>

			<div class="part-form">

				<h3>2. Work experience</h3>

				<div class="form-container" >
					<div id="experience">


						<div id="new-experience">
						<span>Employer</span>
						<input type="text" name="employer[]" />



						<span>Position title</span>
						<input type="text" name="positiontitle[]"/>



						<span>Start date</span>
						<div class="form-date">
							<select class="form-date-month" name="startdatemonth[]">
								<option>-- Month --</option>
								<option value="01">January</option>
								<option value="02">February</option>
								<option value="03">March</option>
								<option value="04">April</option>
								<option value="05">May</option>
								<option value="06">June</option>
								<option value="07">July</option>
								<option value="08">August</option>
								<option value="09">September</option>
								<option value="10">October</option>
								<option value="11">November</option>
								<option value="12">December</option>
							</select>

							<select name="startdateyear[]">
								<option>
									-- Year --
								</option>
									<?php
									for ($i=1970; $i <= date("Y"); $i++) {
										echo "<option value=" . $i . " >" . $i . "</option>";
									}
								 ?>
							</select>
						</div>



						<span>End date</span>
						<div class="form-date">
							<select class="form-date-month" name="enddatemonth[]">
								<option>-- Month --</option>
								<option value="01">January</option>
								<option value="02">February</option>
								<option value="03">March</option>
								<option value="04">April</option>
								<option value="05">May</option>
								<option value="06">June</option>
								<option value="07">July</option>
								<option value="08">August</option>
								<option value="09">September</option>
								<option value="10">October</option>
								<option value="11">November</option>
								<option value="12">December</option>
							</select>

							<select name="enddateyear[]">
								<option>
									-- Year --
								</option>
								<?php
								$end = 1970;
								$start = date("Y");
									for ($i = $start; $i >= $end; $i--) {
										/*$i = rsort($i);*/
										echo "<option value=" . $array[]['year'] = $i . " >" . $array[]['year'] = $i . "</option>";
									}
								 ?>
							</select>
						</div>

					</div>



					</div>



					<a class="add-button" href="javascript: void(0);"  onclick="cloneExperience()">Add work experience</a>


				</div>

				<div class="info-container">

					<b>Work experiences</b><br /> This is where you add your work experiences. Please take your time entering all your work experiences that could be of importance in applying for a specific job. By clicking the button <b>Add new experience</b> you can add unlimited number of experiences.



				</div>

				<div class="form-clr">

				</div>

			</div>



			<div class="part-form">

				<h3> 3. CV & Personal presentation</h3>

				<div class="form-container">

						
						<span>Upload your profile image</span>
						<div class="customfile-container">
  						<input type="file" id="file" name="image" multiple  />
						</div>

						<span>Upload your CV</span>
						<div class="customfile-container">
  						<input type="file" id="file" name="cv" multiple />
						</div>


						<span>Upload personal letter</span>
						<div class="customfile-container">

  						<input type="file" id="file" name="letter" multiple />
						</div>


						<span>Strengths and weaknesses</span>
						<textarea onkeyup="auto_grow(this)" class="presentation" name="presentation"></textarea>

						<div class="customfile-container">
							<input style="margin-left: 34%; margin-top: 20px; width: auto;" type="checkbox" name="terms" value="1" checked required> Yes, I accept <a href="http://www.datainspektionen.se/lagar-och-regler/personuppgiftslagen/" target="_blank">PUL terms and conditions</a>
						</div>
						
					<label class="form-button">
						<span class="button-left"></span>
						<input class="button-save" type="submit" value="Save and continue"/>


				</div>

				<div class="info-container">

				
					<b>CV and personal letter</b><br />
					This is where you upload your profile image, CV, personal letter and even write a short presentation. Entering this information will help our recruiting team to select matching opportunities and contact you with interesting offers. We may even contact you to verify some of the entered information.  



				</div>

				<div class="form-clr">

				</div>


				<input type="hidden" name="option" value="com_agency">
				<input type="hidden" name="task" value="AddNewProfile">

		</form>

	</div>

</div>

<script type="text/javascript">



			;(function( $ ) {

		  // Browser supports HTML5 multiple file?
		  var multipleSupport = typeof $('<input/>')[0].multiple !== 'undefined',
		      isIE = /msie/i.test( navigator.userAgent );

		  $.fn.customFile = function() {

		    return this.each(function() {

		      var $file = $(this).addClass('customfile'), // the original file input
		          $wrap = $('<div class="customfile-wrap">'),
		          $input = $('<input type="text" class="customfile-filename" />'),
		          // Button that will be used in non-IE browsers
		          $button = $('<button type="button" class="customfile-upload">Browse</button>'),
		          // Hack for IE
		          $label = $('<label class="customfile-upload" for="'+ $file[0].id +'">Open</label>');

		      // Hide by shifting to the left so we
		      // can still trigger events
		      $file.css({
		        position: 'absolute',
		        left: '-9999px'
		      });

		      $wrap.insertAfter( $file )
		        .append( $file, $input, ( isIE ? $label : $button ) );

		      // Prevent focus
		      $file.attr('tabIndex', -1);
		      $button.attr('tabIndex', -1);

		      $button.click(function () {
		        $file.focus().click(); // Open dialog
		      });

		      $file.change(function() {

		        var files = [], fileArr, filename;

		        // If multiple is supported then extract
		        // all filenames from the file array
		        if ( multipleSupport ) {
		          fileArr = $file[0].files;
		          for ( var i = 0, len = fileArr.length; i < len; i++ ) {
		            files.push( fileArr[i].name );
		          }
		          filename = files.join(', ');

		        // If not supported then just take the value
		        // and remove the path to just show the filename
		        } else {
		          filename = $file.val().split('\\').pop();
		        }

		        $input.val( filename ) // Set the value
		          .attr('title', filename) // Show filename in title tootlip
		          .focus(); // Regain focus

		      });

		      $input.on({
		        blur: function() { $file.trigger('blur'); },
		        keydown: function( e ) {
		          if ( e.which === 13 ) { // Enter
		            if ( !isIE ) { $file.trigger('click'); }
		          } else if ( e.which === 8 || e.which === 46 ) { // Backspace & Del
		            // On some browsers the value is read-only
		            // with this trick we remove the old input and add
		            // a clean clone with all the original events attached
		            $file.replaceWith( $file = $file.clone( true ) );
		            $file.trigger('change');
		            $input.val('');
		          } else if ( e.which === 9 ){ // TAB
		            return;
		          } else { // All other keys
		            return false;
		          }
		        }
		      });

		    });

		  };

		  // Old browser fallback
		  if ( !multipleSupport ) {
		    $( document ).on('change', 'input.customfile', function() {

		      var $this = $(this),
		          // Create a unique ID so we
		          // can attach the label to the input
		          uniqId = 'customfile_'+ (new Date()).getTime(),
		          $wrap = $this.parent(),

		          // Filter empty input
		          $inputs = $wrap.siblings().find('.customfile-filename')
		            .filter(function(){ return !this.value }),

		          $file = $('<input type="file" id="'+ uniqId +'" name="'+ $this.attr('name') +'"/>');

		      // 1ms timeout so it runs after all other events
		      // that modify the value have triggered
		      setTimeout(function() {
		        // Add a new input
		        if ( $this.val() ) {
		          // Check for empty fields to prevent
		          // creating new inputs when changing files
		          if ( !$inputs.length ) {
		            $wrap.after( $file );
		            $file.customFile();
		          }
		        // Remove and reorganize inputs
		        } else {
		          $inputs.parent().remove();
		          // Move the input so it's always last on the list
		          $wrap.appendTo( $wrap.parent() );
		          $wrap.find('input').focus();
		        }
		      }, 1);

		    });
		  }

		}( jQuery ));

		jQuery('input[type=file]').customFile();


		function cloneExperience() {
			// Get the last <li> element ("Milk") of <ul> with id="myList2"
			var itm = document.getElementById("new-experience");

			// Copy the <li> element and its child nodes
			var cln = itm.cloneNode(true);

			var random = Math.floor((Math.random() * 100000) + 1);

			cln.setAttribute("id", random);
			cln.setAttribute("class", "margin-top");
			// Append the cloned <li> element to <ul> with id="myList1"
			document.getElementById("experience").appendChild(cln);




			var a = document.createElement("a");


			a.setAttribute("id", random + "close");
			a.setAttribute("class", "remove-btn")

			a.setAttribute('href', 'javascript:void(0)');
			a.setAttribute('onclick', 'removeExperience(\''  + random + '\')');
			a.innerHTML = 'Remove experience';
			console.log(a);
			document.getElementById("experience").appendChild(a);
			//div.appendChild(node);
			//var element = document.getElementById("experience");
			//element.appendChild(div);



		}

		function removeExperience(id) {
			var element = document.getElementById('experience');
			element.parentNode.removeChild(element);

			var element = document.getElementById(id + 'close');
			element.parentNode.removeChild(element);

		}

		

</script>
