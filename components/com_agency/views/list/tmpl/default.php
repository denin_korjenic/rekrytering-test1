
<div class="header-client-list">
	<div class="header-wrap">

		<h2>Candidate list</h2>

		<div class="header-client-list-r">


			<form class="header-search" action="<?php echo JURI::root().'client-list';?>" method="post">
				<input type="text" name="keyword" value="" placeholder="Search candidate">
				<input style="vertical-align: top; margin-top: 10px;" type="radio" name="gender" value="">All
				<input style="vertical-align: top; margin-top: 10px;" type="radio" name="gender" value="male">Male
				<input style="vertical-align: top; margin-top: 10px;" type="radio" name="gender" value="female">Female

			</form>

			<a style="display: none;" class="add-new-candidate" href="<?php echo JRoute::_('index.php?option=com_agency&view=editprofile'); ?>">Add new candidate</a>

		</div>
	</div>

</div>

<?php foreach ($this->list as $profile) { ?>

		<div class="profile-list">
			<div class="img-profile" style="border: 3px solid #fff; width: 64px; height: 64px; border-radius: 5px; background-size: cover; background-image: url(<?php echo ($profile->image)?$profile->image:'images/avatar.png';?>)">
				<?php /*if ($profile->image) {
						echo '<img src="' . $profile->image . '" height="64" width="64" />'; 
					}
					else {
						echo '<img src="images/avatar.png" height="64" width="64" />';
					}*/
					?>
			</div>

			<div class="profile-right">

			<?php

			$gender = $profile->gender;

			switch ($gender) {
				case 'male':
					$gender = 'Mr. ';
					break;
				case 'female':
					$gender = 'Mrs. ';
					break;
			}
			?>
				<div class="header-profile">
					<div class="name-profile">
						<a href="<?php echo JRoute::_('index.php?option=com_agency&view=profile&profile=' . $profile->userid); ?>"><?php echo $gender . " " . $profile->name . " " . $profile->lastname; ?></a>
					</div>

					<div class="created">
							<?php echo "(Created: " . date('Y-m-d', strtotime($profile->created)) . ")"; ?>
					</div>

					<div class="edit-link">
						<a href="<?php echo JRoute::_('index.php?option=com_agency&view=edit&profile=' . $profile->userid); ?>">Edit profile</a>
					</div>


				</div>

				<div class="info-profile">


					<?php

		 				echo $profile->country_name . ", " . $profile->city . " | Works at " . $profile->employer . " | Current position: " . $profile->position_title . "<br> Phone: " . $profile->phone . " | E-post: " . $profile->email;

					?>

				</div>

				<div class="files-profile">
					<div class="cv-link">
						<?php if ($profile->cv) { echo '<a class="cv-a" href="' . $profile->cv . '">CV</a>'; } ?>
					</div>

					<div class="letter-link">
						<?php if ($profile->letter) { echo '<a href="' . $profile->letter . '">Letter</a>' ; } ?>
					</div>

					<?php if ($this->comments && $profile->comments_counter > 0 ) { ?>
							<a class="comments" href="#comments<?php echo $profile->userid; ?>" onClick="comments(<?php echo $profile->userid; ?>)">View comments</a>
							<div style="display: none" id="<?php echo 'comments' . $profile->userid; ?>">				
					<?php } else { ?>
					<a class="comments" href="#comments<?php echo $profile->userid; ?>" onClick="comments(<?php echo $profile->userid; ?>)">Add comments</a>
					<div style="display: none" id="<?php echo 'comments' . $profile->userid; ?>">
					<?php } ?>
+
							<h4>Comments</h4>
							<?php foreach ($this->comments as $comment) { ?>
							<?php if ($comment->pid == $profile->userid) { ?>								
									<div class="comment-filed">
										<span style="display: inline-block; font-weight: bold; padding: 4px 0 10px;">
											<?php echo $comment->name  ?>
										</span>
										<span style="display: inline-block; float:right;">
											<?php echo ($comment->date === '0000-00-00 00:00:00')?'':date('Y-m-d G:i', strtotime($comment->date)); ?>
										</span>
										<span style="display: block">
											<?php echo $comment->comment; ?>
										</span>
									</div>
								<?php } ?>
							<?php } ?>
									
						<h4>Add new comment</h4>

						<form action="<?php echo $_SERVER['PHP_SELF'];?>" id="form<?php echo $profile->userid; ?>" name="form<?php echo $profile->userid; ?>" method="post" enctype="multipart/form-data">
							<textarea name="comment" rows="10" cols="30" class="comment-text-field"></textarea>
							<label class="form-button">
								<span class="button-left"></span>
								<input class="button-save" type="submit" value="Post comment"/></label>

							<input type="hidden" name="option" value="com_agency">
							<input type="hidden" name="task" value="AddNewComment">
							<input type="hidden" name="profid" value="<?php echo $profile->userid; ?>" >
						</form>

					</div>

					<div class="clearfix">

					</div>
				</div>

			</div>

			<div class="clear">

			</div>

		</div>

<?php } ?>
<fiv class="pagination-div">
<?php 
$page = JFactory::getApplication()->input->getVar('page',1);
if($page - 2 > 0) echo '<a href="' . JRoute::_('index.php?option=com_agency&view=list&page=' . ($page-1 )) . '" style="padding-right: 10px;"> < </a>';

$start = ($page-2 > 0)?$page-2:1;
$stop = ($page+2 <= $this->pages)?$page+2:$this->pages;
for ($i = $start; $i <= $stop; $i++) {
	if($i == $page) echo '<span class="pagination-active">'.$page.'</span>';
	else echo '<a href="' . JRoute::_('index.php?option=com_agency&view=list&page=' . ($i )) . '" style="padding-right: 10px;">' . ($i ) . '</a>';
}
	if($page + 2 <= $this->pages) echo '<a href="' . JRoute::_('index.php?option=com_agency&view=list&page=' . ($page+1 )) . '" style="padding-right: 10px;"> > </a>';
?>
</div>


</div>


<script type="text/javascript">
<!--
    function comments(id) {
    var element = document.getElementById('comments' + id);

	    if (element) {
	        var display = element.style.display;

	        if (display == "none") {
	            element.style.display = "block";
	        } else {
	            element.style.display = "none";
	        }
	    }
	}

	var identifier = window.location.hash; //gets everything after the hashtag i.e. #home
	if(identifier.length){
		console.log(identifier);

		thenum = identifier.match(/\d+/)[0]
		console.log(thenum);

		comments(thenum);
	}
	

	 // "3"
	/*if (identifier === "#comments") {
	     comments()
	}*///-->
</script>

