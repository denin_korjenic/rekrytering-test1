<!--View profile!-->
<script type="text/javascript">
function auto_grow(element) {
					element.style.height = "5px";
					element.style.height = (element.scrollHeight)+"px";
			}
</script>

<?php 

$profile = $this->profile;

$gender = $profile->gender;

switch ($gender) {
	case 'male':
		$gender = 'Mr. ';
		break;
	case 'female':
		$gender = 'Mrs. ';
		break;
}
?>

<div class="profile">
	<form id="profile" method="post" enctype="multipart/form-data">
		<div class="profile-info-header-single">
			<div class="img-profile">
				<?php 
					JHtml::_('behavior.modal');
						 	if ($profile->image) {
						 		echo '<a href="' . $profile->image . '" class="without-caption image-link modal"><div style="margin: 0px 20px 0px 0px; width: 64px; height: 64px; border-radius: 5px; background-size: cover; background-image: url('.$profile->image.')"> </div></a>';
						 		}
						 	else {
								echo '<a href="images/avatar.png" class="without-caption image-link modal"><div style=" margin: 0px 20px 0px 0px; width: 64px; height: 64px; border-radius: 5px; background-size: cover; background-image: url(images/avatar.png)"> </div></a>';
						} 
					?>
			</div>
			<div class="edit-profile-header-single"><h1><?php echo $gender . $profile->name . " " . $profile->lastname; ?></h1></div>		
		</div>

		<div class="personal-info-container">
			<div class="personal-info">
				<h3 class="personal-info-title">
					<span class="profile-info-nmbr">
						1.
					</span>
					Personal info
				</h3>
				<div class="edit-link"><a href="<?php echo JRoute::_('index.php?option=com_agency&view=edit&profile=' . $profile->userid); ?>">Edit</a></div>
			</div>
			<div class="personal-headers-div"><span class="personal-headers">Name:</span><div class="profile-details"><?php echo $profile->name; ?></div></div>
			<div class="personal-headers-div"><span class="personal-headers">Last name:</span><div class="profile-details"><?php echo $profile->lastname; ?></div></div>
			<div class="personal-headers-div"><span class="personal-headers">Address:</span><div class="profile-details"><?php echo $profile->address; ?></div></div>
			<div class="personal-headers-div"><span class="personal-headers">Zip code:</span><div class="profile-details"><?php echo $profile->zip; ?></div></div>
			<div class="personal-headers-div"><span class="personal-headers">Phone:</span><div class="profile-details"><?php echo $profile->phone; ?></div></div>
			<div class="personal-headers-div"><span class="personal-headers">Place of residence:</span><div class="profile-details"><?php echo $profile->country; ?></div></div>
			<div class="personal-headers-div"><span class="personal-headers">City:</span><div class="profile-details"><?php echo $profile->city; ?></div></div>
			<div class="personal-headers-div"><span class="personal-headers">E-Mail:</span><div class="profile-details"><?php echo $profile->email; ?></div></div>
			<div class="personal-headers-div"><span class="personal-headers">Additional info</span><div class="profile-details"><?php echo $profile->aditional; ?></div></div>
		</div>
		<div class="work-info-container">
			<div class="work-info">
				<h3 class="work-exp-title">
					<span class="profile-info-nmbr">
						2.
					</span>
					Work experience
				</h3>
				<div class="edit-link"><a href="<?php echo JRoute::_('index.php?option=com_agency&view=edit&profile=' . $profile->userid); ?>">Edit</a></div>
			</div>
			<?php foreach ($this->experiance as $key => $exp) { ?>
			
			<div class="wrk-exp-details">
				<div class="personal-headers-div"><span class="personal-headers">Employer:</span><div class="profile-details"><?php echo $exp->employer; ?></div></div>
				<div class="personal-headers-div"><span class="personal-headers">Position title:</span><div class="profile-details"><?php echo $exp->position_title; ?></div></div>
				<div class="personal-headers-div"><span class="personal-headers">Start date:</span><div class="profile-details"><?php echo $exp->start_date; ?></div></div>
				<div class="personal-headers-div"><span class="personal-headers">End date:</span><div class="profile-details"><?php echo $exp->end_date; ?></div></div>
			</div>
			<?php } ?>
		</div>
		<div class="personal-info-container">
			<div class="personal-info">
				<h3 class="personal-info-title">
					<span class="profile-info-nmbr">
						3.
					</span>
					Education
				</h3>
				<div class="edit-link"><a href="<?php echo JRoute::_('index.php?option=com_agency&view=edit&profile=' . $profile->userid); ?>">Edit</a></div>
			</div>
			<div class="personal-headers-div"><span class="personal-headers">Education 1:</span><div class="profile-details"><?php echo $profile->eduone; ?></div></div>
			<div class="personal-headers-div"><span class="personal-headers">Education 2:</span><div class="profile-details"><?php echo $profile->edutwo; ?></div></div>
			<div class="personal-headers-div"><span class="personal-headers">Education 3:</span><div class="profile-details"><?php echo $profile->eduthree; ?></div></div>
		</div>
		<div class="cv-info-container">
			<div class="cv-info">
				<h3 class="work-exp-title">
					<span class="profile-info-nmbr">
						4.
					</span>
					CV & Personal presentation
				</h3>
				<div class="edit-link"><a href="<?php echo JRoute::_('index.php?option=com_agency&view=edit&profile=' . $profile->userid); ?>">Edit</a></div>
			</div>
			<div class="personal-headers-div">
				<span class="personal-headers">Your CV</span>
				<div class="profile-details">
					<?php if ($profile->cv) {
						echo '<a class="cv-a" href="' . $profile->cv . '">CV</a>';
						}
						else {
							echo '<p class="cv-a">No CV</p>';
							} ?>
				</div>
			</div>
			<div class="personal-headers-div">
				<span class="personal-headers">Your personal letter</span>
					<div class="profile-details">
						<?php if ($profile->letter) {
							echo '<a href="' . $profile->letter . '">PL</a>' ;
							}
							else {
								echo '<p>No PL</>' ;
								} ?>
					</div>
			</div>
			<div class="personal-headers-div"><span class="personal-headers">Strengths and weaknesses</span><div class="profile-details-textarea">
				<div class="longInput"> <?php echo $profile->presentation; ?></div>
			</div></div>
			<?php $user = JFactory::getUser();
				$isAdmin = $user->get('isRoot');
				if ($isAdmin) { ?>
					<span>Keywords</span>
					<div class="personal-headers-div"><span class="personal-headers">Keywords</span><div class="profile-details-textarea">
					<div class="longInput"> <?php echo $profile->keywords; ?></div>
			<?php } ?>
			
			</div></div>
			<label class="form-button-cv">
			<span class="button-left"></span>

		</div>

</div>
<?php ?>