<?php

/**
 * @version     1.0.0
 * @package     com_cashback
 * @copyright   Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Izudin Avdibasic <izudin.avdibasic@gmail.com> - http://www.dfunl.com
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Cashback.
 */
class AgencyViewProfile extends JViewLegacy {

    protected $items;
    protected $pagination;
    protected $state;
    protected $params;

    /**
     * Display the view
     */
    public function display($tpl = null) {
      $app = JFactory::getApplication();


      //$this->state = $this->get('State');

      $this->profile = $this->get('Profile');
      $this->experiance = $this->get('Experiance');


      //$this->params = $app->getParams('com_agency');

        //$this->state = $this->get('State');
        //$this->session = $this->get('Data');


        //$this->params = $app->getParams('com_cashback');


        // Check for errors.
        if (count($errors = $this->get('Errors'))) {
;
            throw new Exception(implode("\n", $errors));
        }

        $this->_prepareDocument();
        parent::display($tpl);
    }

    /**
     * Prepares the document
     */
    protected function _prepareDocument() {
        $app = JFactory::getApplication();
        $menus = $app->getMenu();
        $title = null;

        // Because the application sets a default page title,
        // we need to get it from the menu item itself
       /* $menu = $menus->getActive();
        if ($menu) {
            $this->params->def('page_heading', $this->params->get('page_title', $menu->title));
        } else {
            $this->params->def('page_heading', JText::_('COM_CASHBACK_DEFAULT_PAGE_TITLE'));
        }
        $title = $this->params->get('page_title', '');
        if (empty($title)) {
            $title = $app->getCfg('sitename');
        } elseif ($app->getCfg('sitename_pagetitles', 0) == 1) {
            $title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
        } elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
            $title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
        }
        $this->document->setTitle($title);

        if ($this->params->get('menu-meta_description')) {
            $this->document->setDescription($this->params->get('menu-meta_description'));
        }

        if ($this->params->get('menu-meta_keywords')) {
            $this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
        }

        if ($this->params->get('robots')) {
            $this->document->setMetadata('robots', $this->params->get('robots'));
        }*/
        //$this->document->addScript('https://ajax.googleapis.com/ajax/libs/prototype/1.7.0.0/prototype.js');
        //$this->document->addScript('https://ajax.googleapis.com/ajax/libs/scriptaculous/1.9.0/scriptaculous.js');
        //$this->document->addScript(JURI::root().'media/scantool/js/cropper.uncompressed.js');
        //$this->document->addStyleSheet(JURI::root().'media/scantool/js/cropper.css');
       /* $this->document->addStyleSheet(JURI::root().'media/scantool/css/imgareaselect-default.css');
        $this->document->addScript(JURI::root().'media/scantool/js/jquery.min.js');
        $this->document->addScript(JURI::root().'media/scantool/js/jquery.imgareaselect.pack.js');

        $this->document->addStyleSheet(JURI::root().'media/scantool/css/autosuggest_inquisitor.css');
        $this->document->addScript(JURI::root().'media/scantool/js/bsn.AutoSuggest_c_2.0.js');

        $this->document->addStyleSheet(JURI::root().'media/scantool/css/jquery.Jcrop.css');
        $this->document->addScript(JURI::root().'media/scantool/js/jquery.Jcrop.js');*/

    }

}
