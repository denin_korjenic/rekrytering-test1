<?php

/**
 * @version     1.0.0
 * @package     com_mojulaz
 * @copyright   DFUNL Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Dfunl <info@dfunl.com> - http://www.dfunl.com
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Mojulaz records.
 */
class AgencyModelEditProfile extends JModelList
{

	public function getcountryList() {

		$db = JFactory::getDBO();

		$query = $db->getQuery(true);
		$query->select(array('*'))->from('#__agency_countries')->order('country_name');

		$db->setQuery($query);

		$countrylist = $db->loadObjectList();

		return $countrylist;
	}


	public function addNewProfile($object) {


		$db = JFactory::getDBO();
		$db->insertObject('#__agency_profile', $object);


	}

	public function addNewUser($name, $email)
    {

        $mainframe = JFactory::getApplication();
        $user = clone(JFactory::getUser());

        $usersConfig = JComponentHelper::getParams( 'com_users' );


        if(1)
        {
            // Initialize new usertype setting
            jimport('joomla.user.user');
            jimport('joomla.application.component.helper');

            $useractivation = $usersConfig->get('useractivation');

            $db = JFactory::getDBO();
            // Default group, 2=registered
            $defaultUserGroup = 2;

            jimport('joomla.user.helper');
            $password = JUserHelper::genRandomPassword(6);
            $salt     = JUserHelper::genRandomPassword(32);
            $password_clear = $password;

            $crypted  = JUserHelper::getCryptedPassword($password_clear, $salt);
            $password = $crypted.':'.$salt;
            $instance = JUser::getInstance();
            $instance->set('id'         , 0);
            $instance->set('name'           , $name);
            $instance->set('username'       , $email);
            $instance->set('password' , $password);
            $instance->set('password_clear' , $password_clear);
            $instance->set('email'          , $email);
            $instance->set('usertype'       , 'deprecated');
            $instance->set('groups'     , array($defaultUserGroup));
            // Here is possible set user profile details
            $instance->set('profile'    , array());

            // Email with activation link
            if($useractivation == 1)
            {
                $instance->set('block'    , 0);
                $instance->set('activation'    , "");
            }

            if (!$instance->save())
            {
                // Email already used!!!
                // Your code here...
                return 0;
            }
            else
            {
                $db->setQuery("update #__users set email='$email' where username='$email'");
                $db->query();

                $db->setQuery("SELECT id FROM #__users WHERE email='$email'");
                $db->query();
                $newUserID = $db->loadResult();



                // Everything OK!
                if (isset($newUserID))
                {

									return $newUserID;
									/*
                        $emailSubject = 'Your login info';
                        $emailBody = 'Your password is:' . $password_clear;
                        $return = JFactory::getMailer()->sendMail('sender email', 'sender name', $user->email, $emailSubject, $emailBody);
*/
                }
            }


        } else {
            // Registration CLOSED!
            // Your code here...
        }
    }

    public function updateUser($name, $email, $uid)
    {
        $db = JFactory::getDBO();

       $n = new stdClass();
       $n->id = $uid;
       $n->name = $name;
       $n->username = $email;
       $n->email = $email;
       $db->updateObject('#__users',$n,'id');

       return $uid;
    }

    public function updateProfile($object) {

         $db = JFactory::getDBO();
         $query = $db->getQuery(true);
         $query->select(array('*'))->from('#__agency_profile')->where('userid = '.$object->userid);
         $db->setQuery($query);

         $old = $db->loadObject();

        if(isset($object->cv)){
            unlink($old->cv);
        }
        if(isset($object->letter)){
            unlink($old->letter);
        }
        if(isset($object->image)){
            unlink($old->image);
        }

        $db->updateObject('#__agency_profile', $object,'userid');

    }

	public function addNewExperience($id, $object) {

				$end = count($object->employer);

				for ($i=0; $i < $end; $i++) {

					$e = new stdClass();
					$e->employer = $object->employer[$i];
					$e->position_title = $object->position_title[$i];

					$e->start_date = $object->start_dateyear[$i] . "-" . $object->start_datemonth[$i] .  "-01";
					$e->end_date = $object->end_dateyear[$i] . "-" . $object->end_datemonth[$i] . "-01";

					$e->userid = $id;

					$e->id = null;

					$db = JFactory::getDBO();
					$db->insertObject('#__agency_experience', $e);
				}

	}
    public function updateExperience($id, $object) {

                $db = JFactory::getDBO();
                $query = $db->getQuery(true);
 
                // delete all custom keys for user 1001.
                $conditions = array(
                    $db->quoteName('userid') . ' = '.$id
                );
                 
                $query->delete($db->quoteName('#__agency_experience'));
                $query->where($conditions);
                 
                $db->setQuery($query);
                 
                $result = $db->execute();


                $end = count($object->employer);


                for ($i = 0; $i < $end; $i++) {
                    //echo $i;

                    $e = new stdClass();
                    $e->employer = $object->employer[$i];
                    $e->position_title = $object->position_title[$i];

                    $e->start_date = $object->start_dateyear[$i] . "-" . $object->start_datemonth[$i] .  "-01";
                    $e->end_date = $object->end_dateyear[$i] . "-" . $object->end_datemonth[$i] . "-01";

                    $e->userid = $id;

                    $e->id = null;

                    if(strlen($e->employer))
                    $db->insertObject('#__agency_experience', $e);
                }

    }

}
