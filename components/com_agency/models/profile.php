<?php

/**
 * @version     1.0.0
 * @package     com_mojulaz
 * @copyright   DFUNL Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Dfunl <info@dfunl.com> - http://www.dfunl.com
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Mojulaz records.
 */
class AgencyModelProfile extends JModelList
{
	public function getProfile() {

		$db = JFactory::getDBO();

		$app = JFactory::getApplication();
		$input = $app->input;

		$user = JFactory::getUser()->get('id');
		if ($input->getVar("profile")) {
			$user = $input->getVar("profile");
		}

		$query = $db->getQuery(true);
		$query->select(array('*'))->from('#__users AS u')
		->leftJoin('#__agency_profile AS p ON u.id = p.userid')
		->leftJoin('#__agency_countries AS c ON p.country = c.country_code')
		->where('u.id = ' . $user);


		$db->setQuery($query);

		$profile = $db->loadObject();

		return $profile;
	}

	public function getExperiance() {

		$db = JFactory::getDBO();

		$app = JFactory::getApplication();
		$input = $app->input;

		$user = JFactory::getUser()->get('id');

		if ($input->getVar("profile")) {
			$user = $input->getVar("profile");
		}

		$query = $db->getQuery(true);
		$query
			->select(array('*'))
			->from('#__agency_experience AS e')
			->where('e.userid = ' . $user)
			->order('e.start_date DESC');


		$db->setQuery($query);

		$experiance = $db->loadObjectList();

		return $experiance;
	}
	public function removeProfileImage($pid){
		$db = JFactory::getDBO();

		$app = JFactory::getApplication();
		$input = $app->input;

		$query = $db->getQuery(true);
		$query->select(array('*'))->from('#__agency_profile AS p')->where('p.userid = ' . $pid);

		$db->setQuery($query);

		$profile = $db->loadObject();

		unlink($profile->image);

		$profile->image = '';
		$db->updateObject('#__agency_profile',$profile,'userid');
	}
	public function removeProfileLetter($pid){
		$db = JFactory::getDBO();

		$app = JFactory::getApplication();
		$input = $app->input;

		$query = $db->getQuery(true);
		$query->select(array('*'))->from('#__agency_profile AS p')->where('p.userid = ' . $pid);

		$db->setQuery($query);

		$profile = $db->loadObject();

		unlink($profile->letter);

		$profile->letter = '';
		$db->updateObject('#__agency_profile',$profile,'userid');
	}
	public function removeProfileCV($pid){
		$db = JFactory::getDBO();

		$app = JFactory::getApplication();
		$input = $app->input;

		$query = $db->getQuery(true);
		$query->select(array('*'))->from('#__agency_profile AS p')->where('p.userid = ' . $pid);

		$db->setQuery($query);

		$profile = $db->loadObject();

		unlink($profile->cv);

		$profile->cv = '';
		$db->updateObject('#__agency_profile',$profile,'userid');
	}



}
