<?php

/**
 * @version     1.0.0
 * @package     com_mojulaz
 * @copyright   DFUNL Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Dfunl <info@dfunl.com> - http://www.dfunl.com
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Mojulaz records.
 */
class AgencyModelList extends JModelList
{
	public $noElementsPerPage = 20;

	public function getList() {

		$db = JFactory::getDBO();

		$where = 'ugm.group_id <> 8';

		$input = JFactory::getApplication()->input;

		/*if(isset($input->getVar('keyword'))) ;//*/

		if($input->getVar('gender')) $where .= ' AND p.gender = '.$db->Quote($input->getVar('gender'));
		if(strlen($input->getVar('keyword'))) $keywords = explode(' ', $input->getVar('keyword'));
		/*if(strlen($input->getVar('keyword'))){
			$where .= ' AND u.name LIKE '.$db->Quote('%'.$input->getVar('keyword').'%');
			$where .= ' OR p.city LIKE '.$db->Quote('%'.$input->getVar('keyword').'%');
			$where .= ' OR p.lastname LIKE '.$db->Quote('%'.$input->getVar('keyword').'%');
			$where .= ' OR e.position_title LIKE '.$db->Quote('%'.$input->getVar('keyword').'%');
		}*/
		foreach ($keywords as $key => $keyword) {
			$where .= ' AND (u.name LIKE '.$db->Quote('%'.$keyword.'%');
			$where .= ' OR p.city LIKE '.$db->Quote('%'.$keyword.'%');
			$where .= ' OR p.lastname LIKE '.$db->Quote('%'.$keyword.'%');
			$where .= ' OR p.keywords LIKE '.$db->Quote('%'.$keyword.'%');
			$where .= ' OR p.eduone LIKE '.$db->Quote('%'.$keyword.'%');
			$where .= ' OR p.edutwo LIKE '.$db->Quote('%'.$keyword.'%');
			$where .= ' OR p.eduthree LIKE '.$db->Quote('%'.$keyword.'%');
			$where .= ' OR e.position_title LIKE '.$db->Quote('%'.$keyword.'%').')';
		}

		$query = $db->getQuery(true);
		$query->select(array('*','u.id AS userid'))->from('#__users AS u')
		->leftJoin('#__agency_profile AS p ON u.id = p.userid')
		->leftJoin('#__agency_countries AS c ON p.country = c.country_code')
		->leftJoin('#__agency_experience AS e ON p.userid = e.userid')
		->leftJoin('#__user_usergroup_map AS ugm ON u.id = ugm.user_id')
		->where($where)
		->group('u.id')
		->order('u.registerDate DESC');

		$page = JFactory::getApplication()->input->getVar("page", 1) - 1;


		$db->setQuery($query, $page*$this->noElementsPerPage, $this->noElementsPerPage);

		$list = $db->loadObjectList();

		return $list;

	}

	public  function getPages() {
		
		$db = JFactory::getDBO();

		$query = $db->getQuery(true);


		$where = 'ugm.group_id <> 8';

		$input = JFactory::getApplication()->input;


		if(strlen($input->getVar('keyword'))) $keywords = explode(',', $input->getVar('keyword'));
		
		if(strlen($keywords[0])){
			$where .= ' AND u.name LIKE '.$db->Quote('%'.$keywords[0].'%');
		}
		if(strlen($keywords[2])){
			$where .= ' AND p.city LIKE '.$db->Quote('%'.$keywords[2].'%');
		}
		if(strlen($keywords[1])){
			$where .= ' AND p.lastname LIKE '.$db->Quote('%'.$keywords[1].'%');
		}
		if(strlen($keywords[3])){
			$where .= ' AND e.position_title LIKE '.$db->Quote('%'.$keywords[3].'%');
		}
		
		$query->select(array('*','u.id AS userid'))->from('#__users AS u')
		->leftJoin('#__agency_profile AS p ON u.id = p.userid')
		->leftJoin('#__agency_countries AS c ON p.country = c.country_code')
		->leftJoin('#__agency_experience AS e ON p.userid = e.userid')
		->leftJoin('#__user_usergroup_map AS ugm ON u.id = ugm.user_id')
		->where($where)
		->group('u.id');

		
		$db->setQuery($query);
		$total = $db->loadObjectList();
		$total = count($total);
		$pages = ceil($total / $this->noElementsPerPage);
		return $pages;

	}

	public function getcountryList() {

		$db = JFactory::getDBO();

		$query = $db->getQuery(true);
		$query->select(array('*'))->from('#__agency_countries')->order('country_name');

		$db->setQuery($query);

		$countrylist = $db->loadObjectList();

		return $countrylist;
	}

	public function insertComment($comment, $pid) {
		$db = JFactory::getDBO();
		$app = JFactory::getApplication();
		$newComment = new stdClass();
		$newComment->id = null;
		$newComment->uid = JFactory::getUser()->get('id');
		$newComment->pid = $pid;		
		$newComment->comment = $comment;
		$commentCounter = JFactory::getUser()->get('comments_counter');

		if ($commentCounter) {
			$commentCounter++;
		}
		
		$db->insertObject('#__agency_comments', $newComment);
	}
	
	public function getComments() {
	
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select(array('*'))->from('#__agency_comments AS p')
		->leftJoin('#__users AS u ON p.uid = u.id')
		->leftJoin('#__agency_profile AS c ON p.pid = c.userid');	
		$db->setQuery($query);
		$comments = $db->loadObjectList();
		return $comments;

	
	}

}
