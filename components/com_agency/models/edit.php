<?php

/**
 * @version     1.0.0
 * @package     com_mojulaz
 * @copyright   DFUNL Copyright (C) 2015. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 * @author      Dfunl <info@dfunl.com> - http://www.dfunl.com
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');


class AgencyModelEdit extends JModelList
{

	public function getProfile()
    {
        $db = JFactory::getDBO();

        $app = JFactory::getApplication();
        $input = $app->input;

        $query = $db->getQuery(true);
        $query
            ->select(array('*'))
            ->from('#__users AS u')
            ->leftJoin('#__agency_profile AS p ON u.id = p.userid')
            ->leftJoin('#__agency_countries AS c ON p.country = c.country_code')
            ->where('u.id = ' . $input->getVar("profile"));

            $db->setQuery($query);

            $profile = $db->loadObject();

            return $profile;
    }

    public function getExperiance()
    {
        $db = JFactory::getDBO();

        $app = JFactory::getApplication();
        $input = $app->input;

        $query = $db->getQuery(true);
        $query
        ->select(array('*'))
        ->from('#__agency_experience AS e')
        ->where('e.userid = ' . $input->getVar("profile"));

        $db->setQuery($query);

        $experiance = $db->loadObjectList();

        return $experiance;
    }
    
    public function getcountryList() {

        $db = JFactory::getDBO();

        $query = $db->getQuery(true);
        $query->select(array('*'))->from('#__agency_countries')->order('country_name');

        $db->setQuery($query);

        $countrylist = $db->loadObjectList();

        return $countrylist;
    }


}
